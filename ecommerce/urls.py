"""ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path


from products.views import (
        APIHomeView,
        CategoryListAPIView,
        CategoryRetrieveAPIView,
        ProductListAPIView,
        ProductRetrieveAPIView,

    )

from orders.views import (
                    AddressSelectFormView, 
                    UserAddressCreateView,
                    UserAddressCreateAPIView,
                    UserAddressListAPIView,
                    UserCheckoutAPI,
                    OrderList,
                    OrderListAPIView, 
                    OrderDetail,
                    OrderRetrieveAPIView,
                    )

from carts.views import (
        CartAPIView,
        CartView, 
        CheckoutAPIView,
        CheckoutFinalizeAPIView,
        CheckoutView, 
        CheckoutFinalView,
        ItemCountView, 
        )

urlpatterns = [
    path('admin/', admin.site.urls),
]


#API Patterns
urlpatterns += [
    url(r'^api/$', APIHomeView.as_view(), name='home_api'),
    url(r'^api/products/$', ProductListAPIView.as_view(), name='products_api'),
    url(r'^api/products/(?P<pk>\d+)/$', ProductRetrieveAPIView.as_view(), name='products_detail_api'),
    #orders
    url(r'^api/orders/$', OrderListAPIView.as_view(), name='orders_api'),
    url(r'^api/orders/(?P<pk>\d+)/$', OrderRetrieveAPIView.as_view(), name='order_detail_api'),
    #cart
    url(r'^api/cart/$', CartAPIView.as_view(), name='cart_api'),
]


if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
